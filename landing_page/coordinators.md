## Lecturers
#### Martin Matzner
<img src="/IntroCS/landing_page/MaMa.png" width="250pt"> <br>
Professor Matzner studied Information Systems at the Westfälische Wilhelms-Universität Münster (WWU) and at the Turku School of Economics from 2002 to 2007. He then worked first as a research assistant and then as a temporary academic advisor at the Institute for Information Systems at WWU, where he most recently headed the "Service Science" competence center of the European Research Center for Information Systems (ERCIS). Martin Matzner received his doctorate in 2012 for his work on Business Process Management in Service Networks at WWU. After his habilitation, WWU granted him a teaching license for Information Systems in 2016. Martin Matzner has held the chair for Digital Industrial Service Systems at FAU since 2017.
Professor Matzner's research focuses on IT-supported services, business process management and creative information systems research. (https://www.wiso.rw.fau.de/forschung/forschungsprofil/professorenschaft/prof-dr-martin-matzner/ + DeepL)

#### Andreas Harth
<img src="/IntroCS/landing_page/AnHa.jpg" width="250pt"> <br>
Professor Harth trained as a banker and then studied computer science. He then earned his doctorate at the Digital Enterprise Research Institute at the National University of Ireland and his habilitation at the Karlsruhe Institute of Technology. Teaching and research stays took him to the universities of Heidelberg, Innsbruck, Stanford and Southern California. Since 2018, Prof. Harth has held the Chair of Business Information Systems, in particular Technical Information Systems, at the University of Erlangen-Nuremberg, together with a head of department at Fraunhofer IIS-SCS in Nuremberg.
The research interest of Prof. Harth concerns the development of methods and technologies for decentralized information systems, e.g. in the World Wide Web or in blockchain environments, as well as their use in companies. In numerous research projects with industry participation he has investigated the integration of data, e.g. using technologies of the Semantic Web and Linked Data, as well as the interaction of components, e.g. using process modeling languages. Applications of these methods and technologies can be found in the Internet of Things and in the Web of Things, as well as around the topic Industry 4.0. (https://www.wiso.rw.fau.de/forschung/forschungsprofil/professorenschaft/prof-dr-andreas-harth/ + DeepL)

## Section Teachers

#### Sebastian Dunzer
<img src="/IntroCS/landing_page/SeDu.JPG" width="250pt"> <br>
Sebastian Dunzer is a doctorate candidate at the Chair of Digital Industrial Service Systems. 
He studied Information Systems and International Information Systems at FAU. 
He is obliged with the coordination of the module. 
If you have questions, concerns or feedback regarding the course do not hesitate to contact him via e-mail: sebastian.dunzer@fau.de 


#### Daniel Schraudner

Daniel Schraudner is a doctorate candidate at the Chair of Technical Information Systems. 
He studied Computer Science at FAU and is now teaching some of the sections of this introductory course.

## Tutors
#### Maximilian Harl
<img src="/IntroCS/landing_page/MaHa.jpg" width="250pt"> <br>
Maximilian Harl sometimes studies Information Systems at the Friedrich-Alexander University Erlangen-Nuremberg.


#### Christoper Göhl
Christpher Göhl studies International Information Systems since 2019. He has been among the first to excel in Introduction to Computer Science, which is why he has decided to pass his experiences on to new students in the International Information Systems program in his IntroCS Tutorium. 