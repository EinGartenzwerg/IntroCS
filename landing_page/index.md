---
title: IntroCS
---

# Hello, world!
[IntroCS]((https://www.is.rw.fau.de/lehre/veranstaltungen/intro-to-cs/)) is an introductory course for computer science. The institute for information systems offers the course at the Friedrich-Alexander University Erlangen-Nuremberg. We tailored the course for the master's degree program in [International Information Systems]((http://iis.fau.de/)) and therein for students who have little or no background in computer science. The course conveys content of the chair of digital industrial service systems ([FAU](https://www.fau.de/)), CS50x ([Harvard](https://www.harvard.edu/)) and 6.001 ([MIT](http://web.mit.edu/)).
In this course, we supply students with a comprehensive introduction to the fundamentals and computer science discipline. 

Students will learn programming in different languages, i.e., C, Python, and JavaScript. We teach fundamentals, including algorithms, abstraction, data structures, graphs theory, and web technologies. By the end of the semester, students will have a richer understanding of the key principles of the discipline of computer science. Students will be able to speak intelligently about how computers work and how they enable us to become better problem-solvers and hopefully communicate that knowledge to others.

## About this course
**Learning objectives**: After taking this course, students will be able to
- recognise and deduce patterns among programming problems
- decompose problems into parts and compose solutions to these
- operate at multiple levels of abstraction
- infer from first principles how computer systems work
- assess correctness, design, and style of code
- teach yourself new programming languages
- read code documentation and conclude specifications
- describe symptoms of problems precisely and ask questions clearly
- test solutions to problems, find faults and identify corner cases

**How to IntroCS?**:
- Lectures: Lecture Videos, a Hybrid Lecture, and a Guest Talk deliver the main content.
- Shorts: Videos of about 3-30 minutes that dive into a specific topic more deeply.
- Activation Quizzes: Short quizzes that ask about the theoretical concepts from the lecture.
- Exercises/Preliminary Problems: These problems can be used by the students to get a first contact with the content and its application from the lectures/shorts.
- Problem Sets: Problem Sets need to be submitted weekly for grading by the students.
- Bonus Points: We grant bonus points for the final examination based on Quiz and Problem Set performance.

## Academic Honesty
This course's philosophy on academic honesty is best stated as "be reasonable." The course recognizes that interactions with classmates and others can facilitate mastery of the course's material. However, there remains a line between enlisting the help of another and submitting the work of another. This policy characterizes both sides of that line.
The essence of all work that students submit to this course must be their own. Collaboration on problems is not permitted except to the extent that students may ask classmates and others for help so long as that help does not reduce to another doing their work for them. Generally speaking, when asking for help, students may show their code to others, but they may not view their peers', so long as they respect this policy's other constraints.
Acts considered not reasonable by the course are handled harshly.

## Acknowledgement
We thank Harvard's CS50 and MIT's CS6.001 for their open course ware.